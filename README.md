# :exclamation:  [**Download ready-made scripts here**](https://gitlab.com/Firestar99/yolol/-/jobs/artifacts/master/browse/build/yolol/?job=build)

&nbsp; &nbsp; &nbsp;

# Firestar99's yolol script repo
This is where I'll put all my yolol script projects. My main projects:

* [compass](https://gitlab.com/Firestar99/yolol/-/tree/master/src/compass): The Compass Project
* [math](https://gitlab.com/Firestar99/yolol/-/tree/master/src/math): math utilities, mostly how to do complex math on low grade chips

## Folder structure

* `/src`: all plugins are here with source code, documentation and sample ships
* `/plugin`: a derpy made gradle plugin to automate compiling and optimizing yolol, a bit too unstable for general use atm
* `/yodk`: [yodk](https://github.com/dbaumgarten/yodk/releases/) binaries for windows and linux
