package yolol.compiler;

import org.apache.tools.ant.taskdefs.condition.Os;
import org.gradle.api.file.RegularFile;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import org.gradle.work.InputChanges;
import yolol.compiler.incremental.IncrementalTask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class YololCompiler extends IncrementalTask {
	
	public static final String EXTENSION_YOLOL = ".yolol";
	public static final String EXTENSION_NOLOL = ".nolol";
	public static final String INCLUDE = "include";
	
	@TaskAction
	protected void work(InputChanges inputChanges) {
		incremental(inputChanges, inputNorm -> {
			try {
				String fileName;
				boolean nolol;
				if (inputNorm.endsWith(EXTENSION_YOLOL)) {
					fileName = inputNorm.substring(0, inputNorm.length() - EXTENSION_YOLOL.length());
					nolol = false;
				} else if (inputNorm.endsWith(EXTENSION_NOLOL)) {
					fileName = inputNorm.substring(0, inputNorm.length() - EXTENSION_NOLOL.length());
					nolol = true;
				} else {
					return null;
				}
				
				String outputNorm = fileName + ".yolol";
				RegularFile output = getOutputDirectory().get().file(outputNorm);
				File input = findInput(inputNorm);
				
				mkdirsOutput(output.getAsFile().getParentFile());
				getProject().exec(spec -> {
					spec.executable(getProject().getLayout().getProjectDirectory().file("yodk/" + getYodkExec()));
					spec.args(nolol ? "compile" : "optimize", input, "-o", getOutputDirectory().get().file(outputNorm));
				}).rethrowFailure();
				
				String[] inputs = Files.readAllLines(findInput(inputNorm).toPath()).stream().map(t -> {
					t = t.trim();
					if (t.startsWith(INCLUDE)) {
						t = t.substring(INCLUDE.length()).trim();
						if (t.startsWith("\"") && t.endsWith("\""))
							return t.substring(1, t.length() - 1);
					}
					return null;
				}).filter(Objects::nonNull).toArray(String[]::new);
				
				return new Dependency(inputNorm, inputs, new String[] {output.toString()});
			} catch (IOException e) {
				throw error("IO Error reading " + inputNorm + ": " + e.getMessage());
			}
		});
	}
	
	public static String getYodkExec() {
		if (Os.isFamily(Os.FAMILY_WINDOWS)) {
			return "yodk.exe";
		} else if (Os.isFamily(Os.FAMILY_UNIX)) {
			return "yodk";
		} else {
			throw new RuntimeException("Unknown OS");
		}
	}
	
	//no incremental, always full recompile
	AtomicInteger modCounter = new AtomicInteger();
	
	@Input
	public int getMod() {
		return modCounter.incrementAndGet();
	}
}
