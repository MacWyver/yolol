package yolol.compiler.incremental;

import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.FileType;
import org.gradle.api.file.RegularFile;
import org.gradle.api.tasks.Classpath;
import org.gradle.api.tasks.CompileClasspath;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.LocalState;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.SkipWhenEmpty;
import org.gradle.work.ChangeType;
import org.gradle.work.FileChange;
import org.gradle.work.Incremental;
import org.gradle.work.InputChanges;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class IncrementalTask extends DefaultTask {
	
	public static final int MKDIR_RETRY_CNT = 100;
	
	public static class Dependency {
		
		final String filePath;
		final String[] inputs;
		final String[] outputs;
		
		public Dependency(String filePath, String[] inputs, String[] outputs) {
			this.filePath = filePath;
			this.inputs = inputs;
			this.outputs = outputs;
		}
	}
	
	@FunctionalInterface
	public interface WorkFunction {
		
		@Nullable Dependency work(String normalizedPath) throws RuntimeException;
	}
	
	//object
	@InputFiles
	@Classpath
	@SkipWhenEmpty
	public abstract @NotNull ConfigurableFileCollection getSource();
	
	@InputFiles
	@CompileClasspath
	@Incremental
	public abstract @NotNull ConfigurableFileCollection getClasspath();
	
	@OutputDirectory
	public abstract @NotNull DirectoryProperty getOutputDirectory();
	
	@Internal
	public abstract @NotNull DirectoryProperty getCacheDir();
	
	/**
	 * map: changed source files -> classes to recompile
	 * used to figure out which classes need recompilation based on which input files changed
	 */
	@LocalState
	public RegularFile getInputCache() {
		return getCacheDir().get().file("input.cache");
	}
	
	/**
	 * map: computed classes -> previous output files
	 * used to remove output files which are no longer generated
	 */
	@LocalState
	public RegularFile getOutputCache() {
		return getCacheDir().get().file("output.cache");
	}
	
	public File findInput(String normalizedFile) {
		for (File file : getSource()) {
			File f = new File(file, normalizedFile);
			if (f.exists())
				return f;
		}
		for (File file : getClasspath()) {
			File f = new File(file, normalizedFile);
			if (f.exists())
				return f;
		}
		throw error("File " + normalizedFile);
	}
	
	public RuntimeException error(String error) {
		return new RuntimeException(error);
	}
	
	public void mkdirsOutput(File folder) throws IOException {
		for (int i = 0; i < MKDIR_RETRY_CNT; i++) {
			if (folder.exists())
				return;
			//noinspection ResultOfMethodCallIgnored
			folder.mkdirs();
		}
		throw new IOException("After " + MKDIR_RETRY_CNT + " directories could not be created!");
	}
	
	protected void incremental(InputChanges inputChanges, WorkFunction work) {
		ConcurrentLinkedSimpleQueue<String> errors = new ConcurrentLinkedSimpleQueue<>();
		
		//classes to recompute
		ConcurrentMap<String, String[]> inputCache;
		Collection<String> recomputeClasses;
		Set<String> removedClasses;
		if (inputChanges.isIncremental()) {
			//changed classes and classes which used them
			inputCache = readCache(getInputCache());
			List<Check> changes = Stream.concat(
					StreamSupport.stream(inputChanges.getFileChanges(getSource()).spliterator(), true).unordered().map(c -> new Check(c, true)),
					StreamSupport.stream(inputChanges.getFileChanges(getClasspath()).spliterator(), true).unordered().map(c -> new Check(c, false))
			).parallel().unordered().collect(Collectors.toList());
			
			removedClasses = changes
					.stream().parallel().unordered()
					.filter(c -> c.change.getChangeType() == ChangeType.REMOVED && c.change.getFileType() == FileType.FILE)
					.map(c -> c.change.getNormalizedPath())
					.collect(Collectors.toSet());
			
			recomputeClasses = changes
					.stream().parallel().unordered()
					.filter(c -> c.change.getChangeType() != ChangeType.REMOVED && c.change.getFileType() == FileType.FILE)
					.flatMap(c -> {
						String className = c.change.getNormalizedPath();
						removedClasses.remove(className);
						
						Stream<String> thisStream = Stream.ofNullable(c.isSource ? className : null);
						String[] dep = inputCache.get(className);
						if (dep == null)
							return thisStream;
						
						Stream<String> depStream = Arrays.stream(dep).filter(d -> !removedClasses.contains(d));
						return Stream.concat(thisStream, depStream);
					})
					.collect(Collectors.toSet()); //deduplicate
		} else {
			//all classes from getSource()
			inputCache = new ConcurrentHashMap<>();
			removedClasses = Set.of();
			recomputeClasses = StreamSupport
					.stream(inputChanges.getFileChanges(getSource()).spliterator(), true).unordered()
					.filter(e -> e.getFileType() == FileType.FILE)
					.map(FileChange::getNormalizedPath)
					.collect(Collectors.toList());
		}
		
		if (getLogger().isInfoEnabled())
			getLogger().info((inputChanges.isIncremental() ? "running incrementally" : "running fully") + ": [" + String.join(", ", recomputeClasses) + ']');
		
		//compute and write
		List<Dependency> newDependencies = recomputeClasses
				.stream().parallel().unordered()
				.map(normalizedPath -> {
					try {
						return work.work(normalizedPath);
					} catch (RuntimeException e) {
						StackTraceElement[] trace = e.getStackTrace();
						errors.add("\terror processing '" + normalizedPath + "': " + e + "\n\t\tat " + Arrays
								.stream(e.getStackTrace())
								.limit(trace.length - Thread.currentThread().getStackTrace().length + 1)
								.map(StackTraceElement::toString)
								.collect(Collectors.joining("\n\t\tat "))
						);
						return null;
					}
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		
		//update input cache
		//Note: this accumulates potential unnecessary dependencies over time which resets with a rebuild. Shouldn't be too bad.
		newDependencies.stream().parallel().forEach(d -> {
			for (String input : d.inputs) {
				inputCache.compute(input, (key, old) -> {
					//add entry
					if (old == null)
						return new String[] {d.filePath};
					
					//deduplicate
					for (String s : old)
						if (s.equals(d.filePath))
							return old;
					
					//add to array
					String[] news = Arrays.copyOf(old, old.length + 1);
					news[old.length] = d.filePath;
					return news;
				});
			}
		});
		writeCache(getInputCache(), inputCache);
		
		//remove old outputs and update ouput cache
		ConcurrentMap<String, String[]> outputCache = inputChanges.isIncremental() ? readCache(getOutputCache()) : new ConcurrentHashMap<>();
		//noinspection ResultOfMethodCallIgnored
		Stream.concat(
				newDependencies.stream().parallel()
						.filter(d -> d.outputs.length > 0)
						.flatMap(d -> {
							String[] olds = outputCache.put(d.filePath, d.outputs);
							if (olds == null)
								return Stream.empty();
							HashSet<String> news = new HashSet<>(Arrays.asList(d.outputs));
							return Arrays.stream(olds).filter(old -> !news.contains(old));
						}),
				removedClasses.stream().parallel()
						.flatMap(d -> Stream.ofNullable(outputCache.remove(d)))
						.flatMap(Arrays::stream)
		).forEach(normalizedPath -> getOutputDirectory().get().file(normalizedPath).getAsFile().delete());
		writeCache(getOutputCache(), outputCache);
		
		//throw if errors happened
		if (errors.hasEntry())
			throw new GradleException("Errors occured while executing " + getName() + ":\n" + errors.stream().collect(Collectors.joining("\n")));
	}
	
	private static ConcurrentMap<String, String[]> readCache(RegularFile cacheFile) {
		try {
			return Files.readAllLines(cacheFile.getAsFile().toPath()).stream().parallel().unordered()
					.map(s -> s.length() == 0 ? null : s.split("\t"))
					.filter(Objects::nonNull)
					.collect(Collectors.toConcurrentMap(d -> d[0], d -> Arrays.copyOfRange(d, 1, d.length)));
		} catch (IOException e) {
			throw new GradleException("IO error reading cache '" + cacheFile.getAsFile() + "'!", e);
		}
	}
	
	private void writeCache(RegularFile cacheFile, Map<String, String[]> cache) {
		String file = cache
				.entrySet().stream().parallel().unordered()
				.map(entry -> {
					StringBuilder b = new StringBuilder();
					b.append(entry.getKey()).append('\t');
					for (String s : entry.getValue())
						b.append(s).append('\t');
					return b.deleteCharAt(b.length() - 1).toString();
				})
				.collect(Collectors.joining("\n"));
		try {
			Files.writeString(cacheFile.getAsFile().toPath(), file);
		} catch (IOException e) {
			throw new GradleException("IO error writing cache '" + cacheFile.getAsFile() + "'!", e);
		}
	}
	
	private static class Check {
		
		final FileChange change;
		final boolean isSource;
		
		public Check(FileChange change, boolean isSource) {
			this.change = change;
			this.isSource = isSource;
		}
	}
	
	/**
	 * requires separate source configuration
	 */
	public Object configureSymbolGen(FileCollection compileClasspath) {
		getClasspath().from(compileClasspath);
		return getOutputDirectory();
	}
	
	public Object configureDecorate(FileCollection compileClasspath, FileCollection source) {
		getClasspath().from(compileClasspath);
		getSource().from(source);
		return getOutputDirectory();
	}
	
	public Object configurePackage(FileCollection runtimeClasspath) {
		getClasspath().from();
		getSource().from(runtimeClasspath);
		return getOutputDirectory();
	}
}
