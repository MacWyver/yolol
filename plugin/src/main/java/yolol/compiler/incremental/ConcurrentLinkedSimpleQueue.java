package yolol.compiler.incremental;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.VarHandle;

/**
 * A concurrent threadsafe linking based FILO queue.
 */
public class ConcurrentLinkedSimpleQueue<E> implements SimpleQueue<E> {
	
	private static final VarHandle HEAD;
	private static final VarHandle TAIL;
	private static final VarHandle NEXT;
	
	static {
		try {
			Lookup lookup = MethodHandles.lookup();
			HEAD = lookup.findVarHandle(ConcurrentLinkedSimpleQueue.class, "head", Node.class);
			TAIL = lookup.findVarHandle(ConcurrentLinkedSimpleQueue.class, "tail", Node.class);
			NEXT = lookup.findVarHandle(Node.class, "next", Node.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"})
	private volatile @NotNull Node<E> head;
	@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"})
	private volatile @NotNull Node<E> tail;
	
	public ConcurrentLinkedSimpleQueue() {
		Node<E> starterNode = new Node<>(null);
		head = starterNode;
		tail = starterNode;
	}
	
	@Override
	public boolean add(E e) {
		Node<E> node = new Node<>(e);
		//noinspection unchecked
		Node<E> oldTail = (Node<E>) TAIL.getAndSetRelease(this, node);
		NEXT.setOpaque(oldTail, node);
		return true;
	}
	
	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public E remove() {
		while (true) {
			Node<E> head = (Node<E>) HEAD.getAcquire(this);
			Node<E> next = (Node<E>) NEXT.getAcquire(head);
			if (next == null)
				return null;
			
			if (HEAD.weakCompareAndSetRelease(this, head, next)) {
				E item = next.item;
				next.item = null;
				return item;
			}
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean hasEntry() {
		Node<E> head = (Node<E>) HEAD.getAcquire(this);
		Node<E> next = (Node<E>) NEXT.getAcquire(head);
		return next != null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public int size() {
		int i = 0;
		for (Node<E> node = (Node<E>) HEAD.getAcquire(this); node != null; node = (Node<E>) NEXT.getAcquire(node))
			i++;
		return i;
	}
	
	private static class Node<E> {
		
		private @Nullable E item;
		private volatile @Nullable Node<E> next;
		
		public Node(@Nullable E item) {
			this.item = item;
		}
	}
}
