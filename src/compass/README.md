# Compass Project

<img src="docs/ui.jpg" height="300">

**We all know it when your friend sends over some ISAN coordinates and is like *"I found an area with multiple giant asteroids we could mine! Sadly my ship was already loaded with ores."* You want those asteroids, so you make your way to those coords. But actually pointing yourself towards the correct direction? Super hard, near impossible. So you take the most inefficient route, fly around a bunch and after an hour of trying to get there you give up. It's easier to find another highly valuable asteroid field than to find your way to the last one.**

**And this is what Compass tries to solve...**

## Compass provides you with a compass to guide you to whatever ISAN coordinate you want to go to. Never again searching hours for that super precious asteroid you found

* [Download all Yolol scripts from here](https://gitlab.com/Firestar99/yolol/-/jobs/artifacts/master/browse/build/yolol/compass/?job=build)
* [Free Sample labourer ship Blueprint for testing](https://gitlab.com/Firestar99/yolol/-/blob/master/src/compass/ships/Compass_v1_Labourer.fbe)
* [Blueprint containing all Yolol and Memory chips required](https://gitlab.com/Firestar99/yolol/-/blob/master/src/compass/ships/Compass_v1_Components.fbe)



## FAQ
1. **What is ISAN?** [ISAN](isan.to/doc) is a GPS System for Starbase, which shows their users their current location in the large universe. However it doesn't make it easy to actually get to a location you noted down, so that is why Compass was born.
2. **I already have ISAN installed, is that a Problem?** When installing Compass ISAN is no longer required as it comes with it's own ISAN and has a display which is exactly the same as ISAN. But it is also **fully compatible** with a basic and navigation ISAN running next to it. See [Setup -> ISAN compatability](#isan-compatability) for details
3. **Can ISAN or Compass be used by other Players to track where I am?** Tracking other players via ISAN, Compass or any such system is absolutely impossible. The only people who know your location are those who are on your ship and look at that text panel in your cockpit. There has sadly been some bad guys spreading rumors about tracking being possible.
4. **How accurate is compass?** While standing still Compass is very accurate and limited only by the display and the users ability to orient themselves precisely enough. If you are moving however the precision drops considerably with increasing speed, but the further your receivers are located from each other the higher your speed can be while staying precise.
5. **What is the maximum range of Compass?** The same as ISAN range, so around 1000km around origin which is a good distance into belt. But not enough for the very deep belt or to reach all the way to the moon.
6. **What features does ISAN have which Compass doesn't?** It only runs Monos and there is no option for Quads as of right now. Damage on Receivers or Loss of Signal are currently not detected. But everything else like xyz output, speed measurement and streamer mode is supported.
7. **How does Compass compare to [OCEAN](https://github.com/SirBitesalot/OCEAN)?** OCEAN was developed before Compass and computed the rotation of a ship using angles and print them to the user these so that they can align to ISAN coordinates. It currently does not yet feature a compass itself, making navigation for the end-user more difficult than Compass. For a technology discussion see [Technical details](#technical-details). 
8. **My compass is going crazy!** First please slow down until your **ship stands still** and **wait for your compass to stabilize** before reading off Compass. Rotating your ship is already enough disturbance to cause crazyness. If you stand completely still and 10 seconds later it still goes crazy you are welcome to have a look at [Debugging Compass](#debugging-compass).
9. **I've setup compass and it isn't working.** Have a look at [Debugging Compass](#debugging-compass), hopefully that can help you.
10. **Why does Compass use `k` as it's Text output and not `c` like Compass?** ISAN Quad uses the `c` variable, and to keep compatability we cannot use `c`. So I used a letter which sounds similar, `k`.
11. **Can you add Feature X?** Not right now, ask in November 2021 again. I'm currently writing my thesis and should really put some more work into that. Afterwards when I have more time I can look into improving compass. But if any bugs arrive I will still fix them in a timely matter.
12. **Can Compass work together with [Waypoint System](https://github.com/Archaegeo/Starbase/blob/main/ISAN-Waypoint%20System/README.md)?** You should be able to setup both systems on your ship (yes with "double" ISAN setup from both projects) and they should work together, however I never checked it and cannot really support it right now (see point 11 why). Compass reads / writes its target to the same variables as Waypoint does (`wxyz`) so selecting a waypoint there should also allow Compass to read it and inserting a new coordinate from Compass should be visible in Waypoint.
13. **Can Compass work together with NavCas / Autopilot / whatever else?** Not right now, see point 11. If you are the author of one of these systems feel free to adjust your system, submit an MR to Compass, pm me about it and have a look at [Technical details](#technical-details) down below.
14. **What external variables are you using?** This one is for Yolol coders: `:, k, ke, kf, kg, kl, kr, ks, kt` individually, `e, f, g, h, i, j, k, w` * `x, y, z` multiplied together, so `ex, ey, ez, fx, fy...` you get the pattern. Yes that's a lot of variables, but most are double letters and many start with k so shouldn't have too many collisions (except you are using k too of course). See [Technical Details -> Global Variables](#global-variables) for more details.



## How to use an already installed Compass
If you need to setup Compass first, go to [Setup of Compass](#setup-of-compass). Otherwise these 7 simple steps should get you going onto your way to your target:

1. To be able to fly to some target you first have to know your target's location. That can be your friend telling you the location, but if you ever need to know your location just look for your Text Panel with `::  POS  ::` at the top. Here you can see your X, Y and Z coordinates which you can note down or sent to a friend. You'll also see `S:` which is your current speed (in meters per second, or m/s short) and `D:` which is your distance to your target (in meters, or m short) as well as how long it would take to get there with your current speed (in hours and minutes, or h and min short).

<img src="docs/outputxyz.jpg" height="400">

2. To insert your target coordinates you again have to go to your `::  POS  ::` text panel. While looking at it bring up your universal tool (press U), activate your curser (press tab) and switch to the data tab at the top. In the very top row it should have `:` on the left, and some very long text on the right. Click into the right field, press Ctrl+A to select the entire text, press Backspace or Delete to delete the entire text, and now you can insert your coordinates here. They need to be formatted like `x y z` and not contain any decimals (just remove them), so for example `28071 -49196 9337`. Then press enter, close your universal tool (with U) and cursor mode (with tab) and the text panel should say `Parsing new Target...`. After a couple seconds it should go back to normal and show you your current location again.

<img src="docs/target_input.jpg" height="400">

3. Now look at your Compass, which is the text panel starting with `k:` and usually right next to the other terminal. Every time you want to read the compass it is important that your **ship stands still** and you **wait for your compass to stabilize**, otherwise you will get weird data and the target jumps around.

<img src="docs/ui_behind.jpg" height="300">

4. In the image above you can see an example of how a compass could look like. With on the outer `-` and `|` you can see the area the compass operates in as well as the center. You may also see an `O` or `+` somewhere on your screen. These are telling you in which direction you have to rotate to face towards your target. In the case above the `O` is far to the right and a little upwards, so you should rotate your ship to the right (press `e` or `q`) and a little upwards, just like the arrow I've drawn shows you.

5. If you have an `O` it means that your target is behind you (you can't see it). When rotating towards it you will notice how it will move away from the center, this is fully intended to happen. Keep on going in that direction and your `O` will turn into a `+` at some point. Note that the `O` or `+` is able to hide any `-` or `|` on the display but also be shifted over to the right if it would be behind the `k: ` in the top left corner of the screen, so that is always displayed.

<img src="docs/ui_forward.jpg" height="300">

6. Now that you have a `+` the compass is telling you your target is in front of you (you can see it). Rotate your ship in the direction of the `+` until it is in the center of the compass. In the example above the `+` is to the right and a bit downwards, so rotate your ship accordingly.

7. With the `+` now in the center of your compass, your ship is now aligned with your target and you can start flying towards it. It is good practise to every now on then stop and make sure you are still flying towards your target, and are not going off-course. But when checking remember to slow down until your **ship stands still** and **wait for your compass to stabilize** before reading off it, even when you are only rotating your ship.

I hope Compass will save you many hours finding the correct location in this gigantic universe of Starbase :)




## Setup of Compass
To setup a compass in your own ship you need:
* 3 Small Navigation Receivers (large ones also work)
* 3 Hardpoints (to mount Navigation Receivers)
* 2 Basic Yolol Chips
* 2 Advanced Yolol Chip
* 2 Yolol Memory Chips
* 2 Text Panels
* Your choice of: (to hold 6 Yolol chips)
    * 3 Yolol Racks, 3 Yolol Rack Chip Slot (2 Slots) or
    * 3 Yolol Racks, 1 Yolol Rack Chip Slot (2 Slots), 2 Yolol Rack Chip Slot (3 Slots) (with two slots spare for other scripts) or
    * 6 Yolol Chip Sockets (space inefficient)

Some potentially useful blueprints for assembly:

* [Blueprint containing all Yolol and Memory chips required](https://gitlab.com/Firestar99/yolol/-/blob/master/src/compass/ships/Compass_v1_Components.fbe)
* [Free Sample labourer ship Blueprint for testing](https://gitlab.com/Firestar99/yolol/-/blob/master/src/compass/ships/Compass_v1_Labourer.fbe)

### ISAN compatability
If you don't know if you have ISAN or not just ignore all the ISAN compatability extras in the document. Your compass will work regardless of ISAN being present or not.

Compass uses it's own internal modified ISAN and does not require an ISAN to be present on the ship. It actually has a build-in Text Panel which displays ISAN coordinates just like ISAN would so that a seperate ISAN Mono is redundant. It is however fully compatible so that it is possible to install a basic or navigation ISAN next to it, but ISAN won't display the distance to your target (if there are lots of request for this feature I may make a modded ISAN version with support for that). A good reason to install an extra ISAN Quad is the higher coordinate precision while flying. Upgrading from a ship with ISAN to Compass is also possible either by keeping ISAN in the ship and adding Compass additionally or removing ISAN first and using it's components to setup Compass.

### Receivers
The Receivers are the most complex part of the setup and must be installed at the correct locations inside the ship for the compass to operate correctly. Before attempting an install of receivers read through the entire Receiver Chapter first, so you can choose a good location in your ship to install. **The most important thing of a correct setup is how the receivers are positioned relative to each other, not where they are positioned in your ship**. Correctly lining them up is much more important, otherwise you will not be pointing at your target but to the side of it. You should also prioritize placing the receivers as far apart from each other as possible as that greatly increases accuracy while moving, so that you can read Compass even when your ship is not fully standing still, even further apart than shown below would be even better (unlike what is shown in the Reddit release video where the receivers are right next to each other). 

If you never installed a receiver: Place a standard hardpoint on your beams, make sure it's oriented correctly (flat side facing upwards, holes facing towards the beams). If you do this in-game (not in SSC) you want to put in a single bolt. Then place a navigation receiver ontop. On all diagonal sides you can bolt them, the bolts should go though the receiver and the hardpoint straight into the beam below, manual bolting is likely required. Use a wire tool and connect an electrical wire from your network of wires to the bottom of the hardpoint.

The image below showcases a correct setup inside a labourer. Start with Receiver E (red) which is the center receiver and put it into a good location (see below). Then position Receiver F (green) which defines in which direction your ship is facing forwards. Make sure the receiver is placed so that it is directly facing forward, and not slightly sideways, up- or downwards, otherwise your ship will miss it's target. A good way of archiving this is to put all receivers on the same beam "layer", like the bottom floor beams of the Labourer in the image. Now face your camera forwards while standing on the ground. Place the 3rd Receiver G (pink) to the right of the middle Receiver E. Again it is important that this receiver only moves to the right side, and not forwards, upwards or so.

<img src="docs/receivers.jpg" height="300">

With the receivers placed you have to change some of their fields so that the chips can talk to them correctly, similar to how you would setup standard ISAN. In the game you can open your universal tool to configure them in the data tab. In SSC (SpaceShipCreator) you can select the receiver and configure them in the right middle window, but note that you may have to click `>` to go to the next page so you correctly select the `Small Navigation Receiver`. The things you need to change are:
* rename `SignalStrength` to `k` plus the name of the receiver, so `ke`, `kf` or `kg` (in red)
* the value of `ListenAngle` to 180 (in blue)
* rename `TargetMessage` to `kt` (in red)

The image below was taken in SSC, your universal tool on data tab on the left side should have a similar layout, but look differently. Also everything in Yolol is case-insensitive so it doesn't matter whether you name it small `kt` or big `KT` or any combination like `Kt`.

<img src="docs/receivers_fields.jpg" height="300">

### Text Displays
This step will be done completely on the control panels of your Cockpit. What you have to do is place the two Text Panels on your Control Tables, best right in front of you so that you would be able to see the compass when you are flying. If you do not have enough space you can remove some things you don't need, move some of the levers off the table and onto the ground next to the chair (don't forget that now they need to be writed up) or try to use Control Table Plates to extend your control table. Once placed don't forget to bolt the Text Panels (forgetting that happens way too often) and you can setup their names:
* Compass graph: change the `PanelValue` to `k` (for ~~Compass~~ Kompass, you know)
* Position output: change the `PanelValue` to `:`

In the end it should the look something like this:

<img src="docs/text_panels.jpg" height="300">

ISAN Compatability: If you want to remove your ISAN you can rename the `_` panel from ISAN to `:` or `k` whatever you prefer. If you want to have a separate ISAN then just have another panel named `_` somewhere. It will display similar information to Compass `:` but with potential for slightly different xyz and speed values.

### Yolol
Search for some space somewhere in your ship to place all Yolol chips, you'll rarely need to access them at later except for script updates. Place down your option of either 3 Yolol Racks or 6 Yolol Chip Sockets and bolt them down. When using Racks insert all your Yolol Rack Chip Slots and bolt them too. Wire everything up to your electrical network, for Yolol Racks there is a small port on the front of the 2 slot module, for Chip Sockets on the side. Your Socket or Racks should be placed directly next to each other so only one needs to be connected to your electrical network.

[Download all scripts from here](https://gitlab.com/Firestar99/yolol/-/jobs/artifacts/master/browse/build/yolol/compass/?job=build)

Note that you **cannot** make a bookmark directly to the script download link or you won't be receiving the latest updates, you always have to go via this readme (or copy the link without opening the page).

Now copy in the scripts you've got from the link above and put them inside your rack or sockets: (colors are the same as in-game)
* `isan3in.yolol`: basic (white). Communicates with navigation receivers and calculates their location individually using a tripple ISAN mono.
* `delta.yolol`: **advanced chip** (green). Takes the 3 locations of the receivers and calculates your ships rotation, speed as well as compass information.
* `compass.yolol`: basic (white). Takes in compass info from delta and builds the compass display.
* `outputxyz.yolol`: **advanced chip** (green). Displays your xyz coordinates just like ISAN would in addition to speed and distance to target. Also does the parsing of new coordinates.

Now install the two Yolol Memory Chips, which also go into your rack or sockets. For the first one change the names on the left `ChipField1` to `ChipField9` to `wx wy wz kx ky kz kl kr ks`, you probably see the pattern for the first six but the last three ones are a bit special. The last line does not need to be changed as it is unused, as well as the entire right row can be ignored. The second chip needs the names changed to `ex ey ez fx fy fz gx gy gz`, also an easy pattern. See image below for reference on how it should look at the end.

<img src="docs/memory.jpg" height="300">

### Resetting
If you are installing it **in-game, not needed in SSC,** you need to do this one final step to make sure everything works as intended: You will need to go to the `:` text panel which usually now says `:: POS ::` at the top (but doesn't have to) and insert the special word `reset` into there. You do this the same way you enter coordinates, by opening your universal tool on the data tab and change the value (not the name `:`) to `reset`  (see [How to use](#how-to-use-an-already-installed-compass) for more detail if needed). It should display `resetting` and slowly add some points until it switches back to normal.

And there you go, you are done! Now test out if it works properly. If you need some sample coordiates for testing you can use `3881 1435 -15438` in-game and `0 0 0` for SSC which should point you to the warp gate in the middle of the origin stations. Otherwise you can read though the Optional things if you want to, spread the word of Compass existing so that more Endos can enjoy it and say "Hi!" if you ever come across me (Firestar) in the giant universe.

### Optional: Adjust Compass tightness
Compass tightness is how precise the compass can display your heading. By default this setting is at `a=0.7` so that the edge of the compass is reached when you are more than 45 degrees facing sideways away from your target. This is calculated by google-ing `sin(45°)` (don't forget the °) which is `0.707`. Lowering the value will make your compass tighter, so that the center is more precise to offsets from your target but the edge is reached at lower angles. Increasing it will cause the opposite and keeping `a` in the range `(0,1]` (above 0 to 1) is recommended (but technically not required). This option can be adjusted in the `delta.yolol`, the only advanced chip used, right in the first line `a=0.7`. The option may be moved to user input with an update later, let me know if you want that.

### Optional: Export rotation matrix Addon
Requires: 
* 1 Yolol Memory Chip
* 1 free slot for a Yolol Chip (another Chip Socket or more Rack space)

In case you have other scripts which are based upon using Compass to know your ship's rotation you need to have another memory chip to put the computed rotation data. Other tutorials might have either included this in their steps or has directed you to this chapter. Note that you only need to do this step once for your ship so if you install a second script needing rotation data you do **not** repeat this step and just skip it.

To install you just need to slot in the memory chip whereever you have space and configurate the first 9 lines to `ix iy iz jx jy jz kx ky kz`, very similar to how you have done it during Compass installation with the second memory chip but using different names, replacing `efg` with `ijk`.



## Troubleshooting
You compass is not working correctly? Here are some common issues and how to fix them:
* **First always reboot your Compass**: You do this by going to your `:: POS ::` Text Panel and insert `reset` into it just like you would be inserting new coordinates. If you are unsure on how to do that have a look at [Resetting](#resetting)
* **Your Compass looks messed up, symbols are misalighed or the O/+ doesn't show**: try rebooting (see above), then try pasting the chips again and reboot after that.
* **I can't input "reset" or my coords into the text panel**: First make sure it's the text panel with the position output, so the one with the `:: POS ::`. If you paste in reset or coords and it just does nothing make sure you only have a single text panel with `:` as the name as I've noticed that having multiple text panels on `:` has a chance to just never accept any inputs.
* **I have previously used OCEAN**: Note that the receiver configuration currently is different from OCEAN, if you did not change out your receivers please go to [Setup -> Receivers](#receivers) and set them up correctly. SirBitesalot (Author of OCEAN) has however pointed out that he wants to support the receiver configuration of Compass in the future, so this point may be outdated soon.
* **My issue isn't on this list**: I'm a single guy who will not be able to answer everyone and debug everyones setup, sadly. However there are probably many players willing to help in the official Discord in #yolol-code channel. But still please don't flood that immediately and try to find your issue on your own first.
I will be expanding this list as issues come in, but first all the common issues need to be found.



## Licence and Contributors
Compass is licenced under GPL v3 so you are allowed to download, use, modify, redistribute etc. Compass however you like.

* ISAN by Collective: This Project is based upon ISAN v2.5 and would not exist without it, it uses their coordinate system, beacon measurements and triangulation system. [Credits for ISAN](https://github.com/Collective-SB/ISAN#credits)
* Azurethi: They developed ISAN v2 as well as the coordinate parser I am using.
* Firestar: Me, I made Compass and the entire documentation along with it. This Project took all my evenings for about 2 weeks from start to finish, and I'm happy that it is done. (And I can now focus on my Thesis again)

Shoutout to:
* Cylon, the Collaborative Yolol Learning Open Network Discord and it's amazing community!
* SirBitesalot, for developing [OCEAN](https://github.com/SirBitesalot/OCEAN) the potentially first CA attempt at describing a ship's rotation. And inspiring me to commit making Compass.



## Technical details

[Source code](https://gitlab.com/Firestar99/yolol/-/tree/master/src/compass)

[prebuild optimized scripts](https://gitlab.com/Firestar99/yolol/-/jobs/artifacts/master/browse/build/yolol/compass/?job=build)

The prebuild optimized scripts are made with `yodk optimize` and automatically build using ci so that the link will always refer to the latest master version. Note that on branches the link still refers to master and not to the branch.

### Math behind Compass
This will cover everything from receivers, rotation of the ship, building the coordinate system, translating between coord systems until coordinate display output.

<img src="docs/math1.png" height="300">

Compass uses 3 ISAN Monos to figure out the ships orientation, so we draw these as the black points `E F G`. Just like explained in setup `E` is the center receiver, `F` is the front and `G` is to the right side. Now we draw 2 arrows called `h` for forward and `j` for right which go from `E` to their respective receiver and can be calculated with the simple formula below. Note that we normalize these vectors as we do not care about how far each receiver is from each other but only for the direction in which they are. The up vector `i` is calculated using the [cross product](https://www.euclideanspace.com/maths/algebra/vectors/vecAlgebra/cross/index.htm) between `h` and `j` which is why we only need 2 receivers for the 3 directions. Also no need to normalize `i` as the inputs `hj` to the cross product are normalized so the output will be automatically.

<img src="docs/math2.png" height="300">

Now we should define the *absolute* ISAN coordinate system on the left with the 0 point and x y z axis being well `ISAN0=(0,0,0) x=(1,0,0) y=(0,1,0) x=(0,0,1)` as you would expect. Then we can define our ship coordinate system and it's relation to *absolute* ISAN coord system. Going forward any vector in the graph in ship coord system will be in green and every vector in ISAN *absolute* coord system will be black. To define a coord system we need to know the 0 point and either the rotation or the 3 axis of the coord system (you can convert between rotation and axis). The 0 point or position is simple, that's just our `E` point. And for the rotation we define the axis so that x axis = `h`, y axis = `i` and z axis = `j`, or in short our `xyz` axis are the `hij` normalized vectors.

<img src="docs/math3.png" height="300">

So now we add our target point called `W` which is in ISAN coordinate system. To know where it is relative to how our ship is oriented, we want to translate it from *absolute* ISAN coordinate system to our ship coordinate system and call the result `L`. At the bottom left you can see the equasion to calculate `L`: First you substract the position `E` and then do a [vector-matrix-multiplication](https://www.euclideanspace.com/maths/algebra/matrix/transforms/index.htm) with the rotation matrix `m` defined just above.

If you ever need to do the inverse so go from ship coordinate system to *absolute* ISAN coordinate system: do a vector-matrix-multiply using the [transposed matrix](https://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/transpose/index.htm) and add the position `E` to it. A transposed matrix is the inverse matrix for a rotational matrix but easier to compute, litterally just the values mirrored on the diagonal (see link).

Here is the translation in yolol code, feel free to copy and replace the variable names:
```js
wx=:wx wy=:wy wz=:wz //read in w waypoint in ISAN coords
ex=:ex ey=:ey ez=:ez //read in e ship position
hx=:hx hy=:hy hz=:hz ix=:ix iy=:iy iz=:iz jx=:jx jy=:jy jz=:jz //read in rotation matrix

//translate ISAN w -> ship l
wx-=ex wy-=wy wz-=wz //w - e
lx=hx*wx+hy*wy+hz*wz ly=ix*wx+iy*wy+iz*wz lz=jx*wx+jy*wy+jz*wz //l = (w - e) * m
//note that you cannot directly write into wxyz again!

//translate ship l -> ISAN o
ox=hx*lx+ix*ly+jx*lz oy=hy*lx+iy*ly+jz*lz oz=hz*lx+iz*ly+jz*lz //(l * transpose(w))
//note that you cannot directly write into lxyz again!
ox-=ex oy-=ey oz-=ez //o = (l * transpose(w)) + e 

//assert(o == w)
```

To make the compass display we simply take the `L` vector, use the `x` component to calculate `kx` the character compass should use (`O` or `+`) and the `yz` components to calculate the compass position by multiplying by [Compass tightness](#optional-adjust-compass tightness) (default 0.7), min/max to -5,5 for x and -3,3 for y, and add a constant as `ky` and `kz` are directly the line numbers to jump to inside `compass.yolol` and not offsets to save characters.

### Global Variables
* `exyz` means the variables `ex, ey, ez` as a vector.
* If I add new variables in the future they will most likely have the `k`-prefix
* "requires exporting rotation matrix addon": see [Setup -> Optional: Export rotation matrix Addon](#optional-export-rotation-matrix-addon)

|var |writer   |memory |reader               |Description                                                                              |
|----|---------|-------|---------------------|-----------------------------------------------------------------------------------------|
|exyz|isan3in  |memory2|delta                |middle receiver E ISAN position                                                          |
|fxyz|isan3in  |memory2|delta                |front receiver F ISAN position                                                           |
|gxyz|isan3in  |memory2|delta                |right receiver G ISAN position                                                           |
|hxyz|delta    |memory3|                     |forwards (x) direction, part of rotation matrix, requires exporting rotation matrix addon|
|ixyz|delta    |memory3|                     |upwards (y) direction, part of rotation matrix, requires exporting rotation matrix addon |
|jxyz|delta    |memory3|                     |right (z) direction, part of rotation matrix, requires exporting rotation matrix addon   |
|wxyz|outputxyz|memory1|delta                |waypoint, ISAN coordinate input by user, formally called txyz                            |
|kt  |isan3in  |       |receiver             |sets the target message of receivers                                                     |
|ke  |receiver |       |isan3in, outputxyz   |middle receiver E signal strength                                                        |
|kf  |receiver |       |isan3in              |front receiver F signal strength                                                         |
|kg  |receiver |       |isan3in              |right receiver G signal strength                                                         |
|kx  |delta    |memory1|compass              |compass information, character to display                                                |
|ky  |delta    |memory1|compass              |compass information, vertical, line to jump to                                           |
|kz  |delta    |memory1|compass              |compass information, horizontal, line to jump to                                         |
|kl  |delta    |memory1|outputxyz            |distance to target                                                                       |
|kr  |outputxyz|memory1|isan3in, delta       |reset signal, will be set to 20 so `goto 8-:kr` goes to line 1, delta resets compass     |
|ks  |delta    |memory1|outputxyz            |speed of the ship                                                                        |
|k   |compass  |       |text panel           |compass display output                                                                   |
|:   |outputxyz|       |text panel           |ISAN coordinate display output, user input for streamer mode, reset and target coords    |

### Chips Documentation
On cycle time: While some chips (delta specifically) could operate on a lower cycle time there is no reason to speed up processing if your new inputs only comes in every 8 cycles. So first we need to optimize ISAN poll rate, only then does it make sense split delta into two chips for faster processing.

**isan3in**:
* 3 ISAN monos without prediction on a single chip
* cycle time: 2 init + 8 cycle
* based on [ISAN v2.5](https://isan.to/doc) by Collective

**delta**:
* see [Math behind Compass](#math-behind-compass) before reading this code
* calculates rotation matrix for ship space definition
* calculates target direction in ship space
* calculates for compass.yolol lines to jump to
* cycle time: 2 init + 8 cycle (extended to 8 cycle, could be lower)

**compass**:
* compass creates the compass display
* has two lookup tables:
* line 10 to 20: for each character sideways, set lots of variables
* line 3 to 9: for each line, build compass string using custom layout per line and variables set from other table
* these two combine to make compass output
* script just jumps between the two tables
* cycle time: 2 init + 2 cycle


**outputxyz**:
* prints xyz coords, speed, distance to target, time until arrival
* receives text input from user:
* "" -> steamer mode, exit by changing panel
* "reset" -> reset mode, makes all chips run line 1 again
* default -> parses coords "x y z"-style into wxyz
* cycle time: 4 init (extended) + 2 cycle
* xyz coords parser by Azurethi (Collective), [copied from here](https://github.com/Collective-SB/ISAN/blob/bc2d586896e2a34542a98e2771714ce3875e81ec/src/addons/Interface/original/edest.yolol) and modified by me
