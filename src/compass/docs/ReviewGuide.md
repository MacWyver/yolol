# Review Guide for Compass Documentation

I'll keep it short I'll promise! And thanks for helping!

* Standard stuff please give Feedback on everything you notice, just pm send it to me
* Many different kinds of users will read the documentation. It is important to me that all users are served by it, so if you think that user would have trouble understanding that part please notify me. We don't want those users asking questions if we can prevent it with good docs.
* "Introduction", "FAQ" and "How to use an already installed Compass" will be read by users who have just bought their first Ship off the ship shop containing Compass and they want to learn how to use it.
* "Setup of Compass" will be read by users who have bought 1-2 ships, **may** setup ISAN before already and want to either install Compass or "Upgrade" from ISAN.
* "Technical details" is not yet done, but should be understandable by decent Yolol-users. (Will add soon TM)
* And thanks again!
